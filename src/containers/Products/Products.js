import React, {useEffect, useMemo} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {addProduct, fetchProduct, removeProduct, setModalOpen} from "../../store/actions/productActions";
import Product from "../../components/Product/Product";
import './Products.css';
import {DELIVERY} from "../../constants";
import OrderForm from "../../components/OrderForm/OrderForm";
import Modal from "../../UI/Modal/Modal";

const Products = () => {
	const dispatch = useDispatch();
	const products = useSelector(state => state.product.products);
	const productCounts = useSelector(state => state.product.productCounts);
	const total = useSelector(state => state.product.totalPrice);
	const showModal = useSelector(state => state.product.showPurchaseModal);

	useEffect(() => {
		dispatch(fetchProduct());
	}, [dispatch]);

	const add = productType => {
		dispatch(addProduct(productType));
	};

	const deleteProduct = productType => {
		dispatch(removeProduct(productType));
	};

	const purchaseCancelHandler = () => {
		dispatch(setModalOpen(false));
	};

	const purchaseHandler = () => {
		dispatch(setModalOpen(true));
	};

	const purchasable = useMemo(() => {
		const totalIngredients = Object.keys(productCounts)
			.map(type => productCounts[type])
			.reduce((sum, el) => sum + el, 0);

		return totalIngredients > 0;
	}, [productCounts]);

	return products && (
		<div className="MainBlock">
			<Modal
				show={showModal}
				close={purchaseCancelHandler}
			>
				<OrderForm
					closeModal={purchaseCancelHandler}
				/>
			</Modal>
			<div className="Products">
				{products.map(product => (
					<Product
						key={product.id}
						name={product.name}
						price={product.price}
						image={product.image}
						add={() => add(product.id)}
					/>
				))}
			</div>
			<div className="Basket">
				{Object.keys(productCounts).map((p, i) => {
					if (productCounts[p] !== 0) {
						if (p === products[i].id) {
							return (
								<div className="Orders" key={products[i].id}>
									<p onClick={() => deleteProduct(products[i].id)}>{products[i].name}<span className="ProductCount">x{productCounts[p]}</span></p>
									<span>{products[i].price}</span>
								</div>
							);
						}
					}
					return false;
				})}

				<hr/>
				<p>Доставка: {DELIVERY}</p>
				<p>Итого: {total}</p>
				<button className="PlaceOrder" disabled={!purchasable} onClick={purchaseHandler}>Place order</button>
			</div>
		</div>

	);
};

export default Products;