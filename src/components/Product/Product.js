import React from 'react';
import './Product.css';

const Product = props => {
	return (
		<div className="Product">
			<div className="ImageBlock">
				<img src={props.image} alt={props.name} className="img-product"/>
			</div>
			<p>
				{props.name}
				<span><strong>KGS {props.price}</strong></span>
			</p>
			<button type="button" className="Btn" onClick={props.add}>Add to cart</button>
		</div>
	);
};

export default Product;