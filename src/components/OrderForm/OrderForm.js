import React, {useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {createOrder} from "../../store/actions/cartAction";
import './OrderForm.css';

const OrderForm = props => {
	const dispatch = useDispatch();
	const counts = useSelector(state => state.product.productCounts);

	const [customer, setCustomer] = useState({
		name: '',
		email: '',
		street: '',
	});

	const onInputChange = e => {
		const {name, value} = e.target;

		setCustomer(prev => ({
			...prev,
			[name]: value
		}));
	};

	const createOrderToBase = async e => {
		e.preventDefault();

		await dispatch(createOrder({customer, counts}));
		props.closeModal();
	};

	return (
		<div className="OrderFormBlock">
			<form onSubmit={createOrderToBase}>
				<input
					className="Input"
					type="text"
					name="name"
					placeholder="Your Name"
					value={customer.name}
					onChange={onInputChange}
				/>
				<input
					className="Input"
					type="email"
					name="email"
					placeholder="Your Mail"
					value={customer.email}
					onChange={onInputChange}
				/>
				<input
					className="Input"
					type="text"
					name="street"
					placeholder="Street"
					value={customer.street}
					onChange={onInputChange}
				/>
				<button type="submit">Send</button>
			</form>
		</div>
	);
};

export default OrderForm;