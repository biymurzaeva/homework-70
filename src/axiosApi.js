import axios from "axios";

const axiosApi = axios.create({
	baseURL: 'https://javascript-work-project-default-rtdb.firebaseio.com'
});

export default axiosApi;