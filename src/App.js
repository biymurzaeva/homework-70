import './App.css';
import Products from "./containers/Products/Products";

const App = () => (
  <div className="Eat">
    <Products/>
  </div>
);

export default App;
