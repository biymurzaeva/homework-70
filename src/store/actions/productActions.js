import axiosApi from "../../axiosApi";

export const FETCH_PRODUCT_REQUEST = 'FETCH_PRODUCT_REQUEST';
export const FETCH_PRODUCT_SUCCESS = 'FETCH_PRODUCT_SUCCESS';
export const FETCH_PRODUCT_FAILURE = 'FETCH_PRODUCT_FAILURE';

export const ADD_PRODUCT = 'ADD_PRODUCT';
export const REMOVE_PRODUCT = 'REMOVE_PRODUCT';

export const SET_MODAL_OPEN = 'SET_MODAL_OPEN';

export const setModalOpen = isOpen => ({type: SET_MODAL_OPEN, payload: isOpen});

export const fetchProductRequest = () => ({type: FETCH_PRODUCT_REQUEST});
export const fetchProductSuccess = products => ({type: FETCH_PRODUCT_SUCCESS, payload: products});
export const fetchProductFailure = error => ({type: FETCH_PRODUCT_FAILURE, payload: error});

export const fetchProduct = () => {
	return async (dispatch) => {
		try {
			dispatch(fetchProductRequest());

			const response = await axiosApi.get('/products.json');

			if (response.data === null) {
				dispatch(fetchProductSuccess(''));
			} else {
				const fetchProduct = Object.keys(response.data).map(id => ({...response.data[id], id}));
				dispatch(fetchProductSuccess(fetchProduct));
			}
		} catch (error) {
			dispatch(fetchProductFailure(error));
			throw error;
		}
	}
}

export const addProduct = productType => ({type: ADD_PRODUCT, payload: productType});
export const removeProduct = productType => ({type: REMOVE_PRODUCT, payload: productType});