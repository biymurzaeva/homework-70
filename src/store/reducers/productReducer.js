import {
	ADD_PRODUCT,
	FETCH_PRODUCT_FAILURE,
	FETCH_PRODUCT_REQUEST,
	FETCH_PRODUCT_SUCCESS,
	REMOVE_PRODUCT, SET_MODAL_OPEN
} from "../actions/productActions";

const initialState = {
	products: null,
	productCounts: {
		lagman: 0,
		lepeshka: 0,
		plov: 0,
		samsy: 0,
		shakarap: 0,
	},
	delivery: 150,
	totalPrice: 0,
	showPurchaseModal: false,
	loading: false,
	error: null,
};

const productReducer = (state = initialState, action) => {
	switch (action.type) {
		case FETCH_PRODUCT_REQUEST:
			return {...state, loading: true};
		case FETCH_PRODUCT_SUCCESS:
			return {...state, loading: false, products: action.payload};
		case FETCH_PRODUCT_FAILURE:
			return {...state, loading: false, error: action.payload};
		case ADD_PRODUCT:
			let index = -1;

			for (const productCount in state.productCounts) {
				index++;
				if (state.products[index].id === productCount) {
					return {
						...state,
						totalPrice: state.totalPrice + state.delivery + state.products[index].price * state.productCounts[productCount],
						productCounts: {
							...state.productCounts,
							[action.payload]: state.productCounts[action.payload] + 1
						},
					}
				}
			}
			return state;

		case REMOVE_PRODUCT:
			return {
				...state,
				productCounts: {
					...state.productCounts,
					[action.payload]: 0
				},
			}
		case SET_MODAL_OPEN:
			return {
				...state, showPurchaseModal: action.payload
			};
		default:
			return state;
	}
};

export default productReducer;